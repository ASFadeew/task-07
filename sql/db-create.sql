DROP database IF EXISTS testdb;

CREATE database test2db;

USE test2db;

CREATE TABLE users (
	id INT PRIMARY KEY auto_increment,
	login VARCHAR(10) UNIQUE
);

CREATE TABLE teams (
	id INT PRIMARY KEY auto_increment,
	name VARCHAR(10) UNIQUE
);

CREATE TABLE users_teams (
    id INT PRIMARY KEY auto_increment,
    user_id INT,
    team_id INT,
    UNIQUE (user_id, team_id),
    INDEX(id),

    CONSTRAINT fk_user
        FOREIGN KEY (user_id)
            REFERENCES users (id) ON DELETE CASCADE,
    CONSTRAINT fk_team
        FOREIGN KEY (team_id)
            REFERENCES teams (id) ON DELETE CASCADE
);

INSERT INTO users VALUES (DEFAULT, 'ivanov');
INSERT INTO teams VALUES (DEFAULT, 'teamA');

SELECT * FROM users;
SELECT * FROM teams;
SELECT * FROM users_teams;

