package com.epam.rd.java.basic.task7.db.constants;

public class SQLQueryConstants {
    public static final String INSERT_USER = "INSERT INTO users VALUES(DEFAULT, ?)";
    public static final String INSERT_TEAM = "INSERT INTO teams VALUES(DEFAULT, ?)";
    public static final String FIND_ALL_USERS = "SELECT * FROM users u ORDER BY u.id";
    public static final String FIND_ALL_TEAMS = "SELECT * FROM teams t ORDER BY t.id";
    public static final String GET_USER_BY_NAME = "SELECT * FROM users u WHERE u.login=?";
    public static final String GET_TEAM_BY_NAME = "SELECT * FROM teams t WHERE t.name=?";
    public static final String SET_TEAM_FOR_USER = "INSERT INTO users_teams VALUES(?,?)";
    public static final String GET_USER_TEAMS = "SELECT t.id, t.name FROM teams t INNER JOIN users_teams ut " +
            "ON t.id=ut.team_id WHERE ut.user_id=?";
    public static final String DELETE_TEAM = "DELETE FROM teams t WHERE t.name=?";
    public static final String UPDATE_TEAM = "UPDATE teams t SET t.name=? WHERE t.id=?";
    public static final String DELETE_USER = "DELETE FROM users u WHERE u.id=?";
}
