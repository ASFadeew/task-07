package com.epam.rd.java.basic.task7.db;

import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;

import static com.epam.rd.java.basic.task7.db.constants.SQLQueryConstants.*;


public class DBManager {

	private static DBManager instance;
	private static final Properties PROPERTY = new Properties();
	private static String URL;

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
		try (FileReader reader = new FileReader("app.properties")) {
			PROPERTY.load(reader);
			URL = PROPERTY.getProperty("connection.url");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean insertUser(User user) throws DBException {
		if (Objects.isNull(user)) {
			throw new IllegalArgumentException("Argument 'User' cannot be null");
		}

		try (Connection connection = DriverManager.getConnection(URL);
			 PreparedStatement statement = connection.prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS)) {

			statement.setString(1, user.getLogin());

			try {
				statement.executeUpdate();
				try (ResultSet resultSet = statement.getGeneratedKeys()) {
					if (resultSet.next()) {
						user.setId(resultSet.getInt(1));
					}
					return true;
				}
			} catch (SQLException e) {
				e.printStackTrace();
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Error adding user", e);
		}
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();

		try (Connection connection = DriverManager.getConnection(URL);
			 PreparedStatement statement = connection.prepareStatement(FIND_ALL_USERS)) {

			try (ResultSet resultSet = statement.executeQuery()) {
				while (resultSet.next()) {
					users.add(new User(resultSet.getInt("id"), resultSet.getString("login")));
				}
			}
		} catch (SQLException e) {
			throw new DBException("user search error", e);
		}
		return users;
	}

	public boolean insertTeam(Team team) throws DBException {
		if (Objects.isNull(team)) {
			throw new IllegalArgumentException("Argument 'Team' cannot be null");
		}

		try (Connection connection = DriverManager.getConnection(URL);
			 PreparedStatement statement = connection.prepareStatement(INSERT_TEAM, Statement.RETURN_GENERATED_KEYS)) {

			statement.setString(1, team.getName());

			try {
				statement.executeUpdate();
				try (ResultSet resultSet = statement.getGeneratedKeys()) {
					if (resultSet.next()) {
						team.setId(resultSet.getInt(1));
					}
					return true;
				}
			} catch (SQLException e) {
				e.printStackTrace();
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Error adding team", e);
		}
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();

		try (Connection connection = DriverManager.getConnection(URL);
			 PreparedStatement statement = connection.prepareStatement(FIND_ALL_TEAMS)) {

			try (ResultSet resultSet = statement.executeQuery()) {
				while (resultSet.next()) {
					teams.add(new Team(resultSet.getInt("id"), resultSet.getString("name")));
				}
			}
		} catch (SQLException e) {
			throw new DBException("team search error", e);
		}
		return teams;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		if (Objects.isNull(user) || Objects.isNull(teams)) {
			throw new IllegalArgumentException("Arguments 'User' or 'Team' cannot be null");
		}

		try (Connection connection = DriverManager.getConnection(URL);
			 PreparedStatement statement = connection.prepareStatement(SET_TEAM_FOR_USER)) {

			try {
				connection.setAutoCommit(false);
				statement.setInt(1, user.getId());

				int count = 0;
				for (Team team : teams) {
					statement.setInt(2, team.getId());
					count = count + statement.executeUpdate();
				}
				connection.commit();
				return count > 0;
			} catch (SQLException e) {
				connection.rollback();
				throw e;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("unable to find teams for user", e);
		}
	}

	public User getUser(String login) throws DBException {
		if (Objects.isNull(login)) {
			throw new IllegalArgumentException("Argument 'Login' cannot be null");
		}

		try (Connection connection = DriverManager.getConnection(URL);
			 PreparedStatement statement = connection.prepareStatement(GET_USER_BY_NAME)) {

			statement.setString(1, login);

			try (ResultSet resultSet = statement.executeQuery()) {
				User user = null;
				if (Objects.nonNull(resultSet) && resultSet.next()) {
					user = new User(resultSet.getInt("id"), resultSet.getString("login"));
				}
				return user;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("failed to get user by this login", e);
		}
	}

	public Team getTeam(String name) throws DBException {
		if (Objects.isNull(name)) {
			throw new IllegalArgumentException("Argument 'Team`s name' cannot be null");
		}

		try (Connection connection = DriverManager.getConnection(URL);
			 PreparedStatement statement = connection.prepareStatement(GET_TEAM_BY_NAME)) {

			statement.setString(1, name);

			try (ResultSet resultSet = statement.executeQuery()) {
				Team team = null;
				if (Objects.nonNull(resultSet) && resultSet.next()) {
					team = new Team(resultSet.getInt("id"), resultSet.getString("name"));
				}
				return team;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("failed to get team by this team`s name", e);
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		if (Objects.isNull(user)) {
			throw new IllegalArgumentException("Argument 'User' cannot be null");
		}

		List<Team> teams = new ArrayList<>();

		try (Connection connection = DriverManager.getConnection(URL);
			 PreparedStatement statement = connection.prepareStatement(GET_USER_TEAMS)) {

			statement.setInt(1, user.getId());

			try (ResultSet resultSet = statement.executeQuery()) {
				while (resultSet.next()) {
					teams.add(new Team(resultSet.getInt("id"), resultSet.getString("name")));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("failed to get teams for this User", e);
		}
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		if (Objects.isNull(team)) {
			throw new IllegalArgumentException("Argument 'Team' cannot be null");
		}

		try (Connection connection = DriverManager.getConnection(URL);
			 PreparedStatement statement = connection.prepareStatement(DELETE_TEAM)) {

			statement.setString(1, team.getName());
			int count = statement.executeUpdate();
			return count > 0;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("failed to delete this team", e);
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		if (Objects.isNull(team)) {
			throw new IllegalArgumentException("Argument 'Team' cannot be null");
		}

		try (Connection connection = DriverManager.getConnection(URL);
			 PreparedStatement statement = connection.prepareStatement(UPDATE_TEAM)) {

			statement.setString(1, team.getName());
			statement.setInt(2, team.getId());
			int count = statement.executeUpdate();
			return count > 0;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("failed to update this team", e);
		}
	}

	public boolean deleteUsers(User... users) throws DBException {
		if (Objects.isNull(users)) {
			throw new IllegalArgumentException("Argument 'User' cannot be null");
		}

		try (Connection connection = DriverManager.getConnection(URL);
			 PreparedStatement statement = connection.prepareStatement(DELETE_USER)) {

			connection.setAutoCommit(false);
			try {
				int count = 0;
				for (User user : users) {
					statement.setInt(1, user.getId());
					count = count + statement.executeUpdate();
				}
				connection.commit();
				return count == users.length;
			} catch (SQLException e) {
				connection.rollback();
				throw e;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("failed to delete this user", e);
		}
	}
}
